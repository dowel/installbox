#!/bin/bash
zenity --info --title "Install printer drivers" --text "Make sure you are connected to the Internet before you continue!"

SUDOPASS=$(zenity --entry --hide-text --title "Password for local user kvfg" --text "Enter the password for the local user: ")

(
echo 25
echo "# Connecting ..."
sleep 1

echo 50
echo "# Downloading and installing (will take some time and system may seem unresponsive) ..."

echo -e "$SUDOPASS\n" | sudo -S -s -- apt install task-print-server cups printer-driver-all foomatic-db-engine hp-ppd openprinting-ppds printer-driver-cups-pdf

echo 75
echo "# Configuring ..."
sleep 1

echo 100
echo "# Done!"
sleep 1
) | zenity --title "Waiting for installation to finish ..." --progress --auto-close