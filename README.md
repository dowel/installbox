# Debian Installbox

Ein auf https://salsa.debian.org/installer-team/netboot-assistant basierendes System zur Verteilung von Debian (mit XFCE und noch ein paar anderen Dingen für die tägliche Arbeit) auf "unsere Corona Laptops".

## Denke

Die "Corona-Laptops" nutzen einen lokalen User "kvfg", der in der Schule durch Doppelklick auf das Icon "KvFG Connect" auf seinem Desktop die Verbindung mit dem Schulnetz inklusive mount der Serververzeichnisse durchführt. 

So kann der S zu Hause arbeiten - und in der Schule. 

Weiter können die Laptops, die nicht gleich zu Anfang des Schuljahres den S mitgegeben werden (wer weiß schon, wann der Lockdown kommt?), noch eine Zeit in der Schule / in Lerngruppen / als Klassensatz mit Einschränkungen (siehe unten: Hinweise) genutzt werden. 

Eine VM wurde deswegen als Grundlage gewählt, damit parallel mehr Laptops eingerichtet werden können, indem die VM auf diversen Seeding-Geräten zur Verfügung steht.

Preseeding wurde als Methode gewählt, weil Imaging langsamer schien - und unser Schulimage auf die Anwesenheit unseres zentralen Schulservers angewiesen ist. Dieses Image hätte also komplett überarbeitet werden müssen, was mir umständlicher erschien, als den preseeding-Weg zu gehen.

Und außerdem war da andi, der mich motivierte, unterstützte und mir half, wenn ich vor Wände lief ;-)

## Voraussetzungen

Die Zutaten für das Rezept:

- Seeding-Rechner mit zwei Netzwerkschnittstellen
- VirtualBox auf dem Seeding-Rechner
- Switch (weil die direkte Verbindung zwischen Seeding-NIC und Laptop zickt)
- LAN-Kabel
- ein paar "Corona Laptops"

Download einer fertigen VM - siehe weiter unten.

## Anpassungen 

Mensch will vermutlich an der eigenen Schule keine kvfg-Laptops ausrollen. Hier ein paar Hinweise, wie mensch mit der VM zügig ans Ziel kommen könnte bzw. welche Anpassungen von mir vorgenommen wurden.

Auf der VM 

- Desktopumgebung (hier: XFCE) installieren
- Benutzer (hier:kvfg) einrichten
- dessen Homeverzeichnis nach Wunsch anpassen (Theme, Desktopicons etc. pp.)
- Libreoffice-config einrichten (hier: entsprechende Dokumente für die Fremdsprachen liegen in /home/kvfg/Dokumente)
- Browserconfig überarbeiten (Startseite, Privatsphäreneinstellungen, Addons ...) und ublock für die Videoserverdomains (lehrerfortbildung-bw.de für BBB) deaktivieren
- die Ordner für die Servermounts anlegen (hier: /home/kvfg/ServerG-Tauschverzeichnis und /home/kvfg/ServerG-Homeverzeichnis)
- die .desktop Datei für das Verbindungsskript anpassen - die liegt einmal auf dem Schreibtisch und zusätzlich noch in /home/kvfg/.local/share/applications
- die Skripte im Verzeichnis /root überarbeiten
- die preseed.cfg anpassen

Die Skripte liegen aktuell auf der VM in /root 

- build-skel.sh
- connect2kvfgwlan.sh
- kvfg.service
- kvfgshutdown.sh

und werden von dort für den Rollout zusammen mit dem Home des Benutzers in ein _skel.tar_ verpackt. Das erledigt _build-skel.sh_.

Die _preseed.cfg_ enthält die Befehle für den Rollout auf dem Laptop im Abschnitt *d-i preseed/late_command* die ebenfalls überarbeitet werden müssen, damit vor Ort alles rund läuft.

Im Wesentlichen installiert die _preseed.cfg_ die nötigen Programme und packt das zuvor erstellte _skel.tar_ auf dem Laptop wieder aus (und setzt so die Einstellungen für den user *kvfg* in dessen Homeverzeichnis */home/kvfg/*). Dazu schiebt es die Skripte an die richtige Stelle.

Bitte beachten: die _preseed.cfg_ gibt es in zwei Versionen - eine für Buster, eine für Bullseye (mit zwei Paketen weniger). In den ersten paar Zeilen derselben steht, wo welche hin muss.

Die _kvfg.service_ Datei sorgt dafür, dass evtl. gemountete Serververzeichnisse umounted werden; vor allem aber löscht sie die *lehrer/schueler.nmconnection* Dateien aus */etc/NetworkManager/system-connections/* beim Runterfahren weg, so dass nicht user-A seine Credentials an user-B verleiht und diesem Zugriff auf sein Serververzeichnis gewährt. Durchgeführt wird dieser umount bzw. die Löschung durch _kvfgshutdown.sh_.

Die _connect2kvfgwlan.sh_ stellt die Verbindung mit dem WPA2-Enterprise WLAN in der Schule her und mounted die Serververzeichnisse.

Und dann ist da noch die *installprinters.sh* sowie ihre *KvFG Printers.desktop* Datei dazu. Die liegt in */home/kvfg/.bin/* und der Starter in */home/kvfg/.local/share/applications*. Nutzer, die unbedingt eine Druckerconfig haben wollen, ziehen sich damit die wichtigsten Pakete an Bord. Ist nicht schön, aber es scheint zu tun.

Weiter läuft in der VM ein *Apt-Cacher NG*, so dass nach der Installation eines einzelnen Laptops die Pakete im lokalen Netz vorhanden sind. Die VM / der Seeding-Rechner lässt sich dann ohne Internetverbindung einsetzen - und außerdem muss nicht jeder Laptop seine Pakete aus dem Netz ziehen. Die Pakete, die in der *preseed.cfg* genannt sind, sind also schon da.

## Hinweise

Die Laptops sind nicht mit persistenten Einstellungen gebacken, sondern lassen sich an die persönlichen Wünsche des Benutzenden anpassen, der für die Installation weiterer Programme etc. sudo-Rechte hat. Das beudetet konkret, dass die Laptops nicht im Regelbetrieb in der Schule mit wechselnden Benutzer/innen eingesetzt werden sollten. Denn: Hat user-A Änderungen vorgenommen, dann erlebt user-B diese. Z.B.: Kommt user A auf die Idee, das Loginpasswort für _kvfg_ zu ändern, dann sitzt user-B dumm da :-)

Also: Nichts geht über echtes Imaging (z.B. mit Linbo), wenn die Geräte mit wechselnden Benutzern verwendet werden sollen!

**You have been warned!**

Der Download einer fertigen virtuellen Maschine ist hier möglich: https://cloud.kvfg.de/index.php/s/aXtJQxKccPCm8kX

- user: installbox 
- pasword: muster

Der schon angelegte User kvfg hat diese Credentials:

- user: kvfg
- password: kvfg

Packen des Default Homes für den User kvfg inklusive der in der preseed.cfg referenzierten Skripte:

/root/build-skel.sh

Nach Änderungen an der Bootconfig das hier nicht vergessen:

di-netboot-assistant rebuild-menu

## Links

https://bdjl.de/beehive/?p=10136 (Beschreibung: deprecated; Webseite nicht mehr online)

https://www.bdjl.de/localhost/?p=5459 (technische Infos und Releasinformationen)
