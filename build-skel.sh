#!/bin/bash
# /root/build-skel.sh

cp /root/connect2kvfgwlan.sh /home/kvfg/
cp /root/kvfg.service /home/kvfg/
cp /root/kvfgshutdown.sh /home/kvfg/
cd /home/ ; tar cf /var/lib/tftpboot/d-i/buster/skel.tar kvfg/ ; cd
